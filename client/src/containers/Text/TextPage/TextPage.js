import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Switch, Redirect } from 'react-router-dom';
import * as actions from '../../../store/actions';
import { Grid, Segment } from 'semantic-ui-react';
import TextHistory from '../../../components/Text/TextActivity';
import TextNew from '../TextNew';
import TextGuide from './TextGuide';

class TextPage extends Component {
    componentDidMount() {
        this.props.onFetchText({limit:5});
    }

    componentDidUpdate(prevProps) {
        if (this.props.auth.credits !== prevProps.auth.credits) {
            this.props.onFetchText({limit:5});
        }   
    }

    render() {
        let routes = (
            <Switch>
                <Route exact path="/text"  component={TextGuide} />
                <Route exact  path="/text/new" component={TextNew} />
                <Redirect to="/text" />
            </Switch>
        );
        return (
            <Segment style={{ padding: '8em 0em' }} vertical>
                <Grid container columns='equal' verticalAlign='top'>
                    <Grid.Row stretched>
                    <Grid.Column mobile={16} tablet={16} computer={8} style={{ padding: '0em 0em 8em 2em' }}>
                        {routes}
                    </Grid.Column>
                    <Grid.Column  mobile={16} tablet={16} computer={8} style={{ padding: '0em 0em 0em 2em' }}>
                        <TextHistory auth={this.props.auth} texts={this.props.texts} />
                    </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Segment>
        );
    }
}

const mapStateToProps = state => {
    return {
        texts: state.text.texts,
        auth: state.auth
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchText: (items) => dispatch(actions.fetchText(items))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(TextPage);
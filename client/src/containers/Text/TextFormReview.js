import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as actions from '../../store/actions';
import formFields from './formFields';
import {
    Button,
    Message,
    Icon,
    Segment,
    List
  } from 'semantic-ui-react';


const TextFormReviews = ({ onCancel, formValues, submitText, history }) => {
    const reviewText = _.map(formFields, ({ name, label }) => {
        return (
            <List.Item key={name}>
                {/* <List.Content > */}
                    <List.Header as='a'>{label}: {formValues[name]}</List.Header>
                {/* </List.Content> */}
            </List.Item>
        )
    })
    return (
        <Segment>
        <Message
        header='Welcome to Text Crunch!'
        content='Review'
        />
        <List divided relaxed >
            {reviewText}
        </List>
        <Button.Group>
            <Button onClick={() => submitText(formValues, history)} positive>SEND TEXT</Button>
            <Button.Or />
            <Button onClick={onCancel}>BACK</Button>
        </Button.Group>
        <Message warning>
        <Icon name='help' />
        For security reason&nbsp;your message will be sent as&nbsp;"Test from Text Crunch"
        </Message>
    </Segment>
    )
}

const mapStateToProps = state => {
    return { formValues: state.form.textForm.values };
}

export default connect(mapStateToProps, actions)(withRouter(TextFormReviews));
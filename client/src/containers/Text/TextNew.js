import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import TextForm from './TextForm';
import TextFormReview from './TextFormReview';

class TextNew extends Component {
    state = { showFormReview: false };

    renderContent() {
        if (this.state.showFormReview) {
            return <TextFormReview onCancel={() => this.setState({ showFormReview: false })} />
        }
        return <TextForm onTextSubmit={() => this.setState({ showFormReview: true })} />
    }
    render() {
        return (
            <div>
                {this.renderContent()}
            </div>
        );
    };
};

export default reduxForm({
    form: 'textForm'
})(TextNew);
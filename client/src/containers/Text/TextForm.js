import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import _ from 'lodash';
import { Link } from 'react-router-dom';
import TextFeilds from './TextFields';
import formFields from './formFields';
import {
    Button,
    Message,
    Icon,
    Segment,
    Form
  } from 'semantic-ui-react';

class TextForm extends Component {
    state = { cancelForm: false };

    renderFields() {
        return _.map(formFields, ({ label, name, type }) => {
            return (
                <Field
                    key={name}
                    component={TextFeilds}
                    label={label}
                    type={type}
                    name={name}
                    className={name}
                />
            );
        });
    };



    render() {
        return (
            <Segment>
                <Message
                header='Welcome to Text Crunch!'
                content='Fill out the form below to send message'
                />
                <Form onSubmit={this.props.handleSubmit(this.props.onTextSubmit)}>
                    <Form.Field>{this.renderFields()}</Form.Field>
                    <Button.Group>
                        <Link to="/text"><Button>CANCEL</Button></Link>
                        <Button.Or />
                        <Button type='submit' positive>REVIEW</Button>
                    </Button.Group>
                </Form>
                <Message warning>
                <Icon name='help' />
                For security reason&nbsp;your message will be sent as&nbsp;"Test from Text Crunch"
                </Message>
            </Segment>
        )
    }
}

const validatePhone = value =>
value && !/^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/i.test(value) ?
'Invalid phone number' : undefined

const maxLength = (max) => (value) => value && value.length > max ? "Text must be less than 150 characters!" : undefined;

const validate = (values) => {
    const errors = {};
    errors.toNumber = validatePhone(values.toNumber || '');
    errors.smsBody = maxLength(150)(values.smsBody || '')
    _.each(formFields, ({ name }) => {
        if (!values[name]) {
            errors[name] = 'You must provide a value';
        }
    })
    return errors;
}

export default reduxForm({
    validate,
    form: 'textForm',
    destroyOnUnmount: false
    // initialValues: {
    //     smsBody: "test"
    //   }
})(TextForm);
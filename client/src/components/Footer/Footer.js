import React from 'react';
import {
    Container,
    Grid,
    Header,
    List,
    Segment
  } from 'semantic-ui-react';

export default function Footer() {
  return (
    <Segment vertical className='footBG' style={{ padding: '5em 0em' }}>
                <Container>
                    <Grid divided inverted stackable>
                    <Grid.Row>
                        <Grid.Column width={3}>
                        <Header inverted as='h4' content='About' />
                        <List link inverted>
                            <List.Item as='a'>Sitemap</List.Item>
                            <List.Item as='a'>Contact Us</List.Item>
                            <List.Item as='a'>Religious Ceremonies</List.Item>
                            <List.Item as='a'>Text Crunch Plans</List.Item>
                        </List>
                        </Grid.Column>
                        <Grid.Column width={3}>
                        <Header inverted as='h4' content='Services' />
                        <List link inverted>
                            <List.Item as='a'>TOIP</List.Item>
                            <List.Item as='a'>FAQ</List.Item>
                            <List.Item as='a'>How To Textcrunch</List.Item>
                            <List.Item as='a'>Manage your customers</List.Item>
                        </List>
                        </Grid.Column>
                        <Grid.Column width={7}>
                        <Header as='h4' inverted>
                        &copy; Text Crunch
                        </Header>
                        {/* <p>
                            Extra space for a call to action inside the footer that could help re-engage users.
                        </p> */}
                        </Grid.Column>
                    </Grid.Row>
                    </Grid>
                </Container>
                </Segment>
  )
}

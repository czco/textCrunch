import React from 'react';
import moment from 'moment';
import {
    Table,
    Icon
} from 'semantic-ui-react';


const CreditActivity = (props) => {
    const creditList = props.credits && props.credits.map((credit) => {
        const date = moment(credit.dateRefill).format('MM-DD-YYYY');
        const time = moment(credit.dateRefill).format('hh:mm:ss a');
        return (
        <Table.Row key={credit._id}>
          <Table.Cell>{date}</Table.Cell>
          <Table.Cell>{time}</Table.Cell>
          <Table.Cell>+ {credit.dollar} credits</Table.Cell>
        </Table.Row>
            
        );
    })

    return (
        <Table singleLine celled striped color='orange'>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell colSpan='3'>Credit Refill History</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
    
        <Table.Body>
        {creditList}
        </Table.Body>
      </Table>
    )
}

export default CreditActivity;
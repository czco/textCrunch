import React from 'react';
import {
    Grid,
    Header,
    Image,
    Segment
} from 'semantic-ui-react';

const techList = [
    {tech: 'React JS', img: 'https://arcweb.co/wp-content/uploads/2016/10/react-logo-1000-transparent.png'},
    {tech: 'Redux', img: 'http://www.stickpng.com/assets/images/5848309bcef1014c0b5e4a9a.png'},
    {tech: 'Node js', img: 'https://cdn-images-1.medium.com/max/960/1*pxfq-ikL8zPE3RyGB2xbng.png'},
    {tech: 'Mongo DB', img: 'https://static1.squarespace.com/static/513914cde4b0f86e34bbb954/t/58d2c758725e25221a20ed53/1490208601230/mongodb-logo.png'},
    {tech: 'Passport JS', img: 'https://cdn.glitch.com/project-avatar/0d184ee3-fd8d-4b94-acf4-b4e686e57375.png'},
    {tech: 'Heroku', img: 'https://img.icons8.com/color/260/heroku.png'},
    {tech: 'MLab', img: 'https://mlab.com/company/brand/img/downloads/mLab-logo-onlight.png'},
    // {tech: 'PureText', img: '../../assets/images/'},
    {tech: 'Stripe', img: 'http://rorylewisphotography.com/wp-content/uploads/2013/10/logo-stripe.png'}
]
const About = () => (
    <Segment style={{ padding: '8em 0em' }} vertical>
      <Grid container stackable verticalAlign='middle'>
          <Grid.Row>
            <Grid.Column width={6}>
              <Header as='h3' style={{ fontSize: '2em' }}>
                About Us
              </Header>
                <p style={{ fontSize: '1.33em' }}>
                Text Crunch is a basic MERN Stack TOIP Application that allows the user to send text from node backend by third-party API (PureText).  Text crunch is using Google OAuth for the authentication, accepted payment via Stripe, and deployed on Heroku with development and production environments.
                </p>
            </Grid.Column>
            <Grid.Column floated='right' width={10}>
              <Grid textAlign='center' padded>
                {techList.map(t=> (
                    <Grid.Column key={t.tech} mobile={4} tablet={4} computer={3}>
                    <Image src={t.img} alt={t.tech} fluid verticalAlign='middle'/>
                    </Grid.Column>
                ))}
              </Grid>
            </Grid.Column>
          </Grid.Row>
        </Grid>
    </Segment>
)
 
export default About;
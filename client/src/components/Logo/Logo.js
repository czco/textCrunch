import React from 'react';

import textCrunchLogo from '../../assets/images/logoTextCrunch-sm.png';
import './Logo.css';

const logo = () => (
    <div className="logo">
        {/* <img src={textCrunchLogo} alt="TextCrunch" /> */}
        Text Crunch
    </div>
);

export default logo;
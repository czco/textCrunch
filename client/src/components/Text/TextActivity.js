import React from 'react';
import {
  Table,
  Icon
} from 'semantic-ui-react';
import moment from 'moment';

const TextActivity = (props) => {
    const textList = props.texts && props.texts.map((text) => {
      const date = moment(text.dateSent).format('MM-DD-YY');
      const time = moment(text.dateSent).format('hh:mm:ss a');
      return(
          <Table.Row key={text._id}>
            <Table.Cell>
              <Icon name='text telephone' />{text.toNumber}
            </Table.Cell>
            <Table.Cell>{text.message}</Table.Cell>
            <Table.Cell>{date}</Table.Cell>
          </Table.Row>
      ) 
    })
    return (
      <Table singleLine celled striped color='teal'>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell colSpan='3'>Sent Text History</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
        {textList}
        </Table.Body>
      </Table>
    )
}

export default TextActivity;
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { createStore, applyMiddleware, compose } from 'redux';
import reduxThunk from 'redux-thunk';
import './index.css';
import App from './App';
// import registerServiceWorker from './registerServiceWorker';
import reducers from './store/reducers';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
window.axios = axios;

// const composeEnhancers = process.env.NODE_ENV === 'development' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null || compose;

const store = createStore(reducers, {}, applyMiddleware(reduxThunk));

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>, 
    document.querySelector('#root')
);

// console.log('Stipe', process.env.REACT_APP_STRIPE_KEY)
// console.log('Env is ', process.env.NODE_ENV)

export const FETCH_USER = 'fetch_user';
export const FETCH_TEXT = 'fetch_text';
export const FETCH_CREDIT = 'fetch_credit';
export const TEXT_TOTAL = 'text_total';
export const TEXT_CHART = 'text_chart';
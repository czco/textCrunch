import axios from 'axios';
import * as actionTypes from './actionTypes';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export const fetchUser = () => async dispatch => {
    const res = await axios.get('/api/current_user');
    dispatch({ type: actionTypes.FETCH_USER, payload: res.data });
};

export const handleToken = (token) => async dispatch => {
    const res = await axios.post('/api/stripe', token);
    toast.success("Success Notification !", {
        position: toast.POSITION.TOP_CENTER
      });
    dispatch({ type: actionTypes.FETCH_USER, payload: res.data });
}

export const submitText = (values, history) => async dispatch => {
    try {
        const res = await axios.post('/api/text', values)
        toast.success("Success Notification !", {
            position: toast.POSITION.TOP_CENTER
          });
        history.push('/text');
        dispatch({ type: actionTypes.FETCH_USER, payload: res.data });
      } catch (error) {
        toast(error.message)
        console.log(Object.keys(error), error.message); 
      }
}

export const fetchText = ( items ) => async dispatch => {
    const res = await axios.get('/api/text_activity', {params: items});
    dispatch({ type: actionTypes.FETCH_TEXT, payload: res.data });
};

export const fetchCredit = (items ) => async dispatch => {
    const res = await axios.get('/api/refill_activity', {params: items});
    dispatch({ type: actionTypes.FETCH_CREDIT, payload: res.data });
};

export const textTotal = () => async dispatch => {
    const res = await axios.get('/api/text_total');
    dispatch({ type: actionTypes.TEXT_TOTAL, payload: res.data.total });
}

export const textChart = () => async dispatch => {
    const res = await axios.get('/api/text_month');
    console.log("action Chart", res.data)
    dispatch({ type: actionTypes.TEXT_CHART, payload: res.data });
}



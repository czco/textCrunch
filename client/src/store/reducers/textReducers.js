import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    texts: null,
    total: null,
    chart: null
}

const fetchText = ( state, action ) => {
    return updateObject ( state, { texts: action.payload })
}

const textTotal = ( state, action ) => {
    return updateObject ( state, { total: action.payload })
}

const textChart = ( state, action ) => {
    console.log("payload chart ", action.payload)
    return updateObject ( state, { chart: action.payload})
}

const reducer = ( state = initialState, action ) => {
    switch (action.type){
        case actionTypes.FETCH_TEXT: return fetchText( state, action );
        case actionTypes.TEXT_TOTAL: return textTotal( state, action );
        case actionTypes.TEXT_CHART: return textChart( state, action );
        default: return state;
    }
};

export default reducer;

// export default function(state = null, action) {
//     switch (action.type){
//         case FETCH_TEXT:
//             return action.payload || false;
//         // case TEXT_TOTAL:
//         //     return action.payload || false;
//         default:
//             return state;
//     }
// }
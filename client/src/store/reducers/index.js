import { combineReducers } from 'redux';
import { reducer as reduxForm } from 'redux-form';
import authReducer from './authReducer';
import textReducer from './textReducers';
import creditReducer from './creditReducer';

export default combineReducers({
    auth: authReducer,
    form: reduxForm,
    text: textReducer,
    credit: creditReducer
});
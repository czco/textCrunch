import {FETCH_CREDIT} from '../actions/actionTypes';

export default function(state = null, action) {
    switch (action.type){
        case FETCH_CREDIT:
            return action.payload || false;
        default:
            return state;
    }
}
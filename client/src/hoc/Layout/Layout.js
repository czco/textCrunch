import React, { Component } from 'react';
import { connect } from 'react-redux';
import Nav from '../../components/Nav/Navigation';
import Logo from '../../components/Logo/Logo'
import {
  Button,
  Container,
  Icon,
  Menu,
  Responsive,
  Segment,
  Sidebar,
  Visibility,
} from 'semantic-ui-react';
import Footer from '../../components/Footer/Footer'

const getWidth = () => {
  const isSSR = typeof window === 'undefined'

  return isSSR ? Responsive.onlyTablet.minWidth : window.innerWidth
}

class DesktopContainer extends Component {
  state = {}

  hideFixedMenu = () => this.setState({ fixed: false })
  showFixedMenu = () => this.setState({ fixed: true })

  render() {
    const { children } = this.props
    const { fixed } = this.state

    return (
      <Responsive 
        getWidth={getWidth} 
        minWidth={Responsive.onlyTablet.minWidth}
      >
        <Visibility
          once={false}
          onBottomPassed={this.showFixedMenu}
          onBottomPassedReverse={this.hideFixedMenu}
        >
          <Segment
            textAlign='center'
            style={{ minHeight: 700, padding: '0em 0em' }}
            vertical
          >
            <Menu
              style={{ minHeight: 60 }}
              fixed={fixed ? 'top' : null}
              inverted={!fixed}
              pointing={!fixed}
              // secondary={!fixed}
              size='large'
              className='navBG'
            >
              <Nav />
            </Menu>
            {children}
          </Segment>
        </Visibility>
        <Footer/>
      </Responsive>
    )
  }
}


class MobileContainer extends Component {
  state = {}

  handleSidebarHide = () => this.setState({ sidebarOpened: false })
  handleToggle = () => this.setState({ sidebarOpened: true })
  closeSidebar = () => {
    if (this.state.sidebarOpened) {
      this.setState({ sidebarOpened: false });
    }
  };

  render() {
    const { children } = this.props
    const { sidebarOpened } = this.state

    return (
      <Responsive
        as={Sidebar.Pushable}
        getWidth={getWidth}
        maxWidth={Responsive.onlyMobile.maxWidth}
      >
        <Sidebar
          as={Menu}
          animation='push'
          inverted
          onHide={this.handleSidebarHide}
          vertical
          visible={sidebarOpened}
        >
          <Nav handleClick={this.handleSidebarHide}/>
        </Sidebar>

        <Sidebar.Pusher dimmed={sidebarOpened} onClick={this.closeSidebar}>
          <Segment
            textAlign='center'
            style={{ minHeight: 350, padding: '0em 0em' }}
            vertical
          >
            <Container>
              <Menu pointing secondary size='large'>
                <Menu.Item onClick={this.handleToggle}>
                  <Icon name='sidebar' />
                </Menu.Item>
                <Menu.Item position='right'>
                <Logo/>
                </Menu.Item>
              </Menu>
            </Container>
            {children}
          </Segment>
          <Footer/>
        </Sidebar.Pusher>
      </Responsive>
    )
  }
}

const ResponsiveContainer = ({ children }) => (
  <div>
    <DesktopContainer>{children}</DesktopContainer>
    <MobileContainer>{children}</MobileContainer>
  </div>
)


class Layout extends Component {
    render(){
        return(
            <ResponsiveContainer>
                {this.props.children}     
            </ResponsiveContainer>
        )
    }
}

const mapStateToProps = state => {
    return {
        authData: state.auth,
        isAuth: (state.auth !== null) && (state.auth !== false )
    }
}

export default connect(mapStateToProps)(Layout);
import React, { Component } from 'react';
import {  Route, Switch, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from './store/actions';
import Landing from './components/Landing/Landing';
import Dashboard from './containers/Dashboard/Dashboard'
import TextPage from './containers/Text/TextPage/TextPage';
import Layout from './hoc/Layout/Layout';
import Credit from './containers/Credits/Credit';
import About from './components/About/About';

class App extends Component {
 
  componentDidMount() { this.props.fetchUser() };

  render() {

    let routes = (
      <Switch>
        <Route path="/" exact component={Landing} />
        <Route path="/about" exact component={About} />
        <Redirect to="/" />
      </Switch>
    );

    if (this.props.isAuth) {
      routes = (
        <Switch>
          <Route path="/" exact component={Dashboard} />
          <Route path="/about" exact component={About} />
          <Route path="/credit" component={Credit} />
          <Route path="/text" component={TextPage} />
          <Redirect to="/dashboard" />
        </Switch>
      );
    }
    return (
      <Layout>
        {routes}
      </Layout>
    );
  }
}

const mapStateToProps = state => {
  return {
    authData: state.auth,
    isAuth: (state.auth !== null) && (state.auth !== false )
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    fetchUser: () => {
      dispatch(actions.fetchUser())
    }
  }
}

export default withRouter( connect( mapStateToProps, mapDispatchToProps )( App ) );

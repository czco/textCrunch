const mongoose = require('mongoose');
const { Schema } = mongoose;
const textSchema =  new Schema({
    toNumber: String,
    message: String,
    dateSent: Date,
    _user: { type: Schema.Types.ObjectId, ref: 'User '}
});
mongoose.model('text', textSchema);
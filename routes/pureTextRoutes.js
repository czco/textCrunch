const mongoose = require('mongoose');
const keys = require('../config/keys');
const puretext = require('puretext');
const requireLogin = require('../middlewares/requireLogin');
const requireCredits = require('../middlewares/requireCredits');
const Text = mongoose.model('text');

module.exports = app => {
    app.post('/api/text', requireLogin, requireCredits, async (req, res) => {
        const info = {
            toNumber: req.body.toNumber,
            smsBody: 'Test from Text Crunch',
            fromNumber: '+1-716-265-4051',
            apiToken: keys.pureTextToken
        }

        const text = new Text({
            toNumber: req.body.toNumber,
            message: 'Test from Text Crunch',
            _user: req.user.id,
            dateSent: Date.now()
        })

        try {
            await puretext.send(info);
            await text.save();
            req.user.credits -= 1;
            const user = await req.user.save();
            res.send(user);
        } catch (err) {
            console.log("text err: ",err)
            return res.send(err);
        }
    });

    app.get('/api/text_activity', requireLogin, async (req, res) => {
        const limit = req.query.limit
        const text_activity = await Text.find({ _user: req.user.id }).sort({ 'dateSent': -1 }).limit(parseInt(limit));
        return res.status(200).json(text_activity);
    })

    app.get('/api/text_total', requireLogin, async (req, res) => {
        const text_total = await Text.find({ _user: req.user.id }).count();
        return res.status(200).json({ total: text_total });
    })

    app.get('/api/text_month', requireLogin, async (req, res) => {
        
        const query = [
            { $match:  { _user: mongoose.Types.ObjectId(req.user.id) } },
            {  $project: { month: { $month: "$dateSent" } }  }, 
            {  $group: { _id: "$month",  users: { $sum: 1 }} }
        ]
        
        let text_monthly

        try {
            text_monthly = await Text.aggregate(query)
            //text_monthly = await Text.find({ _user: req.user.id});
            console.log(text_monthly);
        } catch (error) {
            console.error(error);
        }

        return res.status(200).json({ text_monthly })
    })
};